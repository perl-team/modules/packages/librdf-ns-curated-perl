librdf-ns-curated-perl (1.006-1) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Update lintian override info format in d/source/lintian-overrides on line
    2-7.
  * Set upstream metadata fields: Repository-Browse.
  * Update standards version to 4.6.2, no changes needed.

  [ Florian Schlichting ]
  * Import upstream version 1.006.
  * Update upstream metadata (Github Org)
  * Update upstream copyright years

 -- Florian Schlichting <fsfs@debian.org>  Sun, 21 Jan 2024 21:51:09 +0100

librdf-ns-curated-perl (1.004-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper dependency to >= 10, since that's what is used in
    debian/compat.
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 13 Dec 2022 00:42:19 +0000

librdf-ns-curated-perl (1.004-2) unstable; urgency=medium

  * fix watch file

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 12 Dec 2019 19:57:12 +0100

librdf-ns-curated-perl (1.004-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * enable autopkgtest
  * declare compliance with Debian Policy 4.4.1
  * use debhelper compatibility level 10
  * update watch file:
    + use substitution strings
    + rewrite usage comment
  * relax to build-depend unversioned on perl
  * stop build-depend on dh-buildinfo
  * copyright: bump (yes not extend) coverage

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 11 Dec 2019 06:44:52 +0100

librdf-ns-curated-perl (1.001-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).
    + Fix a typo bug in dc11 URI.
    + Add an all method.
    + Add author test to check the RDFa Initial Context.
    + Add csvw prefix.
    + Add sosa prefix.
    + Add qname method.
    + Add solid and acl prefixes.
    Closes: Bug#890246.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/*: update URLs from {search,www}.cpan.org to MetaCPAN.
  * debian/*: update GitHub URLs to use HTTPS.

  [ Jonas Smedegaard ]
  * Simplify rules:
    + Do copyright-check in maintainer script (not during build).
      Stop build-depend on licensecheck.
    + Drop get-orig-source target: Use gbp import-orig --uscan.
    + Stop resolve (build-)dependencies in rules file.
    + Use short-form dh sequencer (not cdbs).
      Stop build-depend on cdbs.
  * Wrap and sort control file.
  * Update watch file: Fix typo in usage comment.
  * Update copyright info:
    + Strip superfluous copyright marks.
    + Extend coverage of packaging.
    + Use https protocol in protocol URL.
    + Extend coverage for main upstream author.
  * Declare compliance with Debian Policy 4.2.1.
  * Set Rules-Requires-Root: no.
  * Stop bogusly depends on ${shlibs:Depends}.
  * Tighten lintian overrides regarding License-Reference.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 02 Nov 2018 22:07:49 +0100

librdf-ns-curated-perl (0.005-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).
    + Add more prefixes.

  [ Jonas Smedegaard ]
  * Modernize Vcs-* fields:
    + Use git subdir.
    + Use https protocol for Vcs-Git.
    + Use .git suffix
  * Declare compliance with Debian Policy 3.9.8.
  * Modernize git-buildpackage config: Filter any .git* file.
  * Update watch file:
    + Bump to file format 4.
    + Watch only MetaCPAN URL.
    + Mention gbp --uscan in usage comment.
    + Tighten version regex.
  * Stop override lintian for
    package-needs-versioned-debhelper-build-depends: Fixed in lintian.
  * Update copyright info:
    + Extend coverage of Debian packaging.
    + Fix cover Makefile.PL.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 23 Jan 2017 18:20:24 +0100

librdf-ns-curated-perl (0.002-2) unstable; urgency=medium

  * Fix build package as arch-independent.
    Closes: Bug#809361. Thanks to Dagfinn Ilmari Mannsåker.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 30 Dec 2015 08:18:06 +0530

librdf-ns-curated-perl (0.002-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#797717.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 26 Dec 2015 17:02:53 +0530
